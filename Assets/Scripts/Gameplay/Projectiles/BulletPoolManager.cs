﻿using System.Collections.Generic;
using UnityEngine;

public class BulletPoolManager : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private int length;
    Queue<GameObject> pool;

    void Start()
    {
        pool = new Queue<GameObject>();

        for (int i = 0; i < length; i++)
        {
            GameObject bulletGO = Instantiate(prefab);
            bulletGO.name = "Bullet " + (i + 1);
            bulletGO.transform.parent = transform;

            SetBulletToPool(bulletGO);
        }
    }

    public GameObject GetBulletFromPool()
    {
        GameObject bulletGO = pool.Dequeue();
        bulletGO.SetActive(true);

        Bullet bullet = bulletGO.GetComponent<Bullet>();
        bullet.destroyed = false;

        return bulletGO;
    }

    public void SetBulletToPool(GameObject bulletGO)
    {
        bulletGO.SetActive(false);
        pool.Enqueue(bulletGO);
    }
}