﻿using UnityEngine;

public class Bullet : Projectile
{
    [SerializeField] private LayerMask turretMask;

    private BulletPoolManager bulletPoolManager;
    [HideInInspector] public Character.Type owner;

    void Awake()
    {
        bulletPoolManager = GameObject.FindGameObjectWithTag("BulletManager").GetComponent<BulletPoolManager>();
        rigid = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!destroyed)
        {
            CollisionSphere();
            Destroy();
        }
    }

    public override void Collision(GameObject collisionGO)
    {
        if (CheckLayerInMask(playerMask, collisionGO.layer))
        {
            Player player = collisionGO.GetComponent<Player>();

            if (owner != Character.Type.Player)
                player.ReceiveDamage(damage);
        }
        else if (CheckLayerInMask(enemyMask, collisionGO.layer))
        {
            Enemy enemy = collisionGO.GetComponent<Enemy>();

            if (owner != Character.Type.Enemy)
                enemy.ReceiveDamage(damage);

            if (enemy.Dead)
            {
                if (owner == Character.Type.Player)
                    Player.OnPlayerScore?.Invoke(enemy.points);
            }
        }
        else if (CheckLayerInMask(turretMask, collisionGO.layer))
        {
            Turret turret = collisionGO.GetComponent<Turret>();
            turret.ReceiveDamage(damage);

            if (turret.Dead)
            {
                if (owner == Character.Type.Player)
                    Player.OnPlayerScore?.Invoke(turret.points);
            }
        }
        else if (CheckLayerInMask(boxMask, collisionGO.layer))
        {
            Box box = collisionGO.GetComponent<Box>();

            if (box != null)
            {
                if (owner == Character.Type.Player)
                    Player.OnPlayerScore?.Invoke(box.points);

                box.Destroy();
            }
        }
        else if (CheckLayerInMask(bombMask, collisionGO.layer))
        {
            Bomb bomb = collisionGO.GetComponent<Bomb>();

            if (bomb != null)
            {
                if (owner == Character.Type.Player)
                    Player.OnPlayerScore?.Invoke(bomb.points);
            }
        }
    }

    public override void Destroy()
    {
        base.Destroy();
        AudioManager.Get().Play("bullet-explotion");
        bulletPoolManager.SetBulletToPool(gameObject);
    }
}