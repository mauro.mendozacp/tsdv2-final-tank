﻿using UnityEngine;

public class Missile : Projectile
{
    private MissilePoolManager missilePoolManager;

    void Awake()
    {
        missilePoolManager = GameObject.FindGameObjectWithTag("MissileManager").GetComponent<MissilePoolManager>();
        rigid = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!destroyed)
        {
            CollisionSphere();
            Destroy();
        }
    }

    override public void Collision(GameObject collisionGO)
    {
        if (CheckLayerInMask(playerMask, collisionGO.layer))
        {
            Player player = collisionGO.GetComponent<Player>();
            player.ReceiveDamage(damage);
        }
        else if (CheckLayerInMask(enemyMask, collisionGO.layer))
        {
            Enemy enemy = collisionGO.GetComponent<Enemy>();
            enemy.ReceiveDamage(damage);
        }
        else if (CheckLayerInMask(boxMask, collisionGO.layer))
        {
            Box box = collisionGO.GetComponent<Box>();

            if (box != null)
                box.Destroy();
        }
    }

    public override void Destroy()
    {
        base.Destroy();
        AudioManager.Get().Play("missile-explotion");
        missilePoolManager.ReturnMissileToPool(gameObject);
    }
}
