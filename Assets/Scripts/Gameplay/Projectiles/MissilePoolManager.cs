﻿using System.Collections.Generic;
using UnityEngine;

public class MissilePoolManager : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private int length;
    Queue<GameObject> pool;

    void Start()
    {
        pool = new Queue<GameObject>();

        for (int i = 0; i < length; i++)
        {
            GameObject missileGO = Instantiate(prefab);
            missileGO.name = "Missile " + (i + 1);
            missileGO.transform.parent = transform;

            ReturnMissileToPool(missileGO);
        }
    }

    public GameObject GetMissileFromPool()
    {
        GameObject missileGO = pool.Dequeue();
        missileGO.SetActive(true);

        Missile missile = missileGO.GetComponent<Missile>();
        missile.destroyed = false;

        return missileGO;
    }

    public void ReturnMissileToPool(GameObject missileGO)
    {
        missileGO.SetActive(false);
        pool.Enqueue(missileGO);
    }
}