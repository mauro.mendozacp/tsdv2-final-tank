﻿using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    [SerializeField] protected GameObject explosion;
    [SerializeField] protected float explosionRadius;
    [SerializeField] protected LayerMask characterMask;
    [SerializeField] protected LayerMask playerMask;
    [SerializeField] protected LayerMask enemyMask;
    [SerializeField] protected LayerMask boxMask;
    [SerializeField] protected LayerMask bombMask;

    protected Rigidbody rigid;
    protected Transform explosionManager;
    protected float explosionDuration = 2f;
    [HideInInspector] public bool destroyed = false;
    [HideInInspector] public float damage = 0f;    

    void Start()    
    {
        explosionManager = GameObject.FindGameObjectWithTag("ExplosionManager").transform;
    }

    virtual public void Destroy()
    {
        GameObject explosionGO = Instantiate(explosion);
        explosionGO.transform.position = transform.position;
        explosionGO.transform.parent = explosionManager;
        Destroy(explosionGO, explosionDuration);

        rigid.velocity = Vector3.zero;
        rigid.angularVelocity = Vector3.zero;
        destroyed = true;
    }

    protected void CollisionSphere()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius, characterMask);

        foreach (Collider col in colliders)
        {
            Collision(col.gameObject);
        }
    }

    abstract public void Collision(GameObject collisionGO);

    protected bool CheckLayerInMask(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
}
