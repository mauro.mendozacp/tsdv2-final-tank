﻿using System;
using UnityEngine;

public class Box : MonoBehaviour
{
    [SerializeField] private GameObject boxOrigin;
    [SerializeField] private GameObject boxShatter;
    [SerializeField] private LayerMask missileMask;
    public int points = 0;
    public bool destroyed = false;

    private Rigidbody rigid;
    private BoxCollider boxCollider;

    public static Action<Box> OnBoxDestroy;

    void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
    }

    public void Destroy()
    {
        if (!destroyed)
        {
            destroyed = false;
            OnBoxDestroy?.Invoke(this);

            rigid.velocity = Vector3.zero;
            boxCollider.enabled = false;
            AudioManager.Get().Play("break-box");

            boxOrigin.SetActive(false);
            boxShatter.SetActive(true);

            Destroy(gameObject, 2f);
        }
    }
}