﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour
{
    public GameObject boxPrefab;
    public int lenght;

    [HideInInspector] public List<Box> boxList = new List<Box>();
    

    void Awake()
    {
        Box.OnBoxDestroy += DestroyBox;
    }

    void OnDestroy()
    {
        Box.OnBoxDestroy -= DestroyBox;
    }

    void DestroyBox(Box b)
    {
        boxList.Remove(b);
        Player.OnPlayerDestroyedBox?.Invoke();
    }
}