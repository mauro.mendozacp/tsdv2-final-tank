﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : MonoBehaviour
{
    [SerializeField] float distance;
    [SerializeField] float angle;
    [SerializeField] Player player;

    private float height;
    private float smoothSpeed = 0.125f;

    void Start()
    {
        height = (Mathf.Tan(angle * Mathf.PI / 180)) * distance;
        FollowPlayer();
    }

    void FixedUpdate()
    {
        FollowPlayer();
    }

    void FollowPlayer()
    {
        Vector3 target = player.transform.position + (player.transform.up * height) - (distance * Vector3.forward);
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, target, smoothSpeed);
        transform.position = smoothedPosition;

        transform.LookAt(player.transform);
    }
}