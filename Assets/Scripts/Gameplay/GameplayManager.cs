﻿using System;
using UnityEngine;

public class GameplayManager : MonoBehaviourSingleton<GameplayManager>
{
    [SerializeField] private BoxManager boxManager;
    [SerializeField] private BombManager bombManager;
    [SerializeField] private EnemyManager enemyManager;
    [SerializeField] private Player player;
    [SerializeField] private float spawnLimit = 75f;
    [SerializeField] private Vector2 playerLimit;

    public float timer = 1f;
    private float baseTimer = 1f;
    private float offset = 15f;
    private bool accelerateMusic = false;

    public override void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        Player.OnPlayerDeath += PlayerDeath;
        GameManager.Get().Init();
    }

    void Start()
    {
        AudioManager.Get().Play("gameplay-music");
        InitBoxes();
        InitBombs();
        InitEnemies();
        baseTimer = timer;
    }

    void Update()
    {
        if (!GameManager.Get().GameOver)
        {
            timer -= Time.deltaTime;
            CheckPlayerLimits();

            if (!accelerateMusic)
            {
                if (timer < baseTimer / 4)
                {
                    accelerateMusic = true;
                    AudioManager.Get().AccelerateMusicGameplay("gameplay-music", true);
                }
            }
            if (timer < 0f)
            {
                GameManager.Get().FinishGame(player, true);
            }
        }
    }

    void OnDestroy()
    {
        Player.OnPlayerDeath -= PlayerDeath;
    }

    void InitBoxes()
    {
        for (int i = 0; i < boxManager.lenght; i++)
        {
            GameObject boxGo = Instantiate(boxManager.boxPrefab);
            Box box = boxGo.GetComponent<Box>();
            boxGo.name = "Box " + (i + 1);

            boxGo.transform.position = GetRandomPosition();
            boxGo.transform.parent = boxManager.transform;

            boxManager.boxList.Add(box);
        }
    }

    void InitBombs()
    {
        for (int i = 0; i < bombManager.lenght; i++)
        {
            GameObject bombGo = Instantiate(bombManager.bombPrefab);
            Bomb bomb = bombGo.GetComponent<Bomb>();
            bombGo.name = "Bomb " + (i + 1);

            bombGo.transform.position = GetRandomPosition();
            bombGo.transform.rotation = Quaternion.LookRotation(Vector3.back, Vector3.up);
            bombGo.transform.parent = bombManager.transform;

            bombManager.bombList.Add(bomb);
        }
    }

    void InitEnemies()
    {
        for (int i = 0; i < enemyManager.lenght; i++)
        {
            GameObject enemyGO = Instantiate(enemyManager.enemyPrefab);
            Enemy enemy = enemyGO.GetComponent<Enemy>();
            enemy.player = player.transform;
            
            enemyGO.name = "Enemy " + (i + 1);
            enemyGO.transform.position = GetRandomPosition();
            enemyGO.transform.parent = enemyManager.transform;

            enemyManager.enemyList.Add(enemy);
        }
    }

    Vector3 GetRandomPosition()
    {
        float posX = UnityEngine.Random.Range(-spawnLimit, spawnLimit);
        float posZ = UnityEngine.Random.Range(-spawnLimit, spawnLimit);
        Vector3 auxPos = transform.position;
        auxPos.x += posX;
        auxPos.z += posZ;

        if (CheckPositionInPlayer(player.transform.position, auxPos))
        {
            auxPos.x += offset * 2f;
            auxPos.z += offset * 2f;
        }

        return auxPos;
    }

    bool CheckPositionInPlayer(Vector3 pos, Vector3 newPos)
    {
        if (pos.x - offset < newPos.x && pos.x + offset > newPos.x &&
            pos.z - offset < newPos.z && pos.z + offset > newPos.z)
        {
            return true;
        }

        return false;
    }

    void CheckPlayerLimits()
    {
        if (!player.Dead)
        {
            if (player.transform.position.x < transform.position.x - playerLimit.x ||
            player.transform.position.x > transform.position.x + playerLimit.x ||
            player.transform.position.z < transform.position.z - playerLimit.y ||
            player.transform.position.z > transform.position.z + playerLimit.y)
            {
                player.Health = 0f;
            }
        }
    }

    void PlayerDeath()
    {
        GameManager.Get().FinishGame(player, false);
    }
}