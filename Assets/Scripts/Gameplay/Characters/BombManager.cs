﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombManager : MonoBehaviour
{
    public GameObject bombPrefab;
    public int lenght;

    [HideInInspector] public List<Bomb> bombList = new List<Bomb>();

    void Awake()
    {
        Bomb.OnBombDestroy += DestroyBomb;
    }

    void OnDestroy()
    {
        Bomb.OnBombDestroy -= DestroyBomb;
    }

    void DestroyBomb(Bomb bomb)
    {
        bombList.Remove(bomb);
    }
}
