﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Turret : Character
{
    public int points = 0;

    [SerializeField] private GameObject body;
    [SerializeField] private GameObject canon;
    [SerializeField] private Canvas canvas;
    [SerializeField] private Image lifeBar;

    [SerializeField] private Transform player;
    [SerializeField] private MissilePoolManager missilePoolManager;
    protected BoxCollider boxCollider;

    void Awake()
    {
        rigid = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();

        canvas.worldCamera = Camera.main;
    }

    void Start()
    {
        baseHealth = Health;
    }

    override public void ReceiveDamage(float damage)
    {
        Health -= damage;
        UpdateLifeBar();
    }

    void FixedUpdate()
    {
        if (!Dead)
            Attack();

        BarFollowCamera();
    }

    void Attack()
    {
        if (!AlreadyAttack)
        {
            AlreadyAttack = true;

            if (enumRotate != null)
            {
                StopCoroutine(enumRotate);
                enumRotate = null;
            }

            enumRotate = AimTarget();
            StartCoroutine(enumRotate);

            Invoke(nameof(ResetAttack), timeAttack);
        }
    }

    private IEnumerator AimTarget()
    {
        Vector3 bodyTarget = (player.position - body.transform.position).normalized;
        Quaternion bodyRot = Quaternion.LookRotation(bodyTarget, transform.up);
        bodyRot.eulerAngles = new Vector3(0f, bodyRot.eulerAngles.y, 0f);

        Vector3 canonTarget = (player.position - canon.transform.position).normalized;
        Quaternion canonRot = Quaternion.LookRotation(canonTarget, transform.up);
        canonRot.eulerAngles = new Vector3(canonRot.eulerAngles.x, bodyRot.eulerAngles.y, 0f);

        while (body.transform.rotation != bodyRot || canon.transform.rotation != canonRot)
        {
            body.transform.rotation = Quaternion.RotateTowards(body.transform.rotation, bodyRot, turnSpeed * Time.deltaTime);

            Vector3 canonEuler = canon.transform.eulerAngles;
            Vector3 destEuler = Quaternion.RotateTowards(canon.transform.rotation, canonRot, aimSpeed * Time.deltaTime).eulerAngles;
            canonEuler.x = destEuler.x;
            canon.transform.eulerAngles = canonEuler;

            yield return new WaitForEndOfFrame();
        }

        ShootMissile();
        yield return null;
    }

    void ShootMissile()
    {
        GameObject missileGO = missilePoolManager.GetMissileFromPool();
        missileGO.transform.position = projectileStart.position;
        missileGO.transform.rotation = projectileStart.rotation;

        Missile missile = missileGO.GetComponent<Missile>();
        missile.damage = damage;

        fireParticle.Play();
        AudioManager.Get().Play("missile-launcher");

        Rigidbody missileRigid = missileGO.GetComponent<Rigidbody>();
        missileRigid.AddForce(projectileStart.forward * projectileSpeed, ForceMode.Impulse);
    }

    void BarFollowCamera()
    {
        canvas.transform.LookAt(Camera.main.transform);
    }

    void UpdateLifeBar()
    {
        lifeBar.fillAmount = Health / baseHealth;
    }

    override public void Death()
    {
        base.Death();
        boxCollider.enabled = false;
        body.SetActive(false);
        Destroy(gameObject, 2f);
    }
}