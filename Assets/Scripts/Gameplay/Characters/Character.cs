﻿using System.Collections;
using UnityEngine;

public class Character : MonoBehaviour, IDamageable
{
    [SerializeField] protected float health;
    [SerializeField] protected float damage;
    [SerializeField] protected float moveSpeed;
    [SerializeField] protected float turnSpeed;
    [SerializeField] protected float aimSpeed;
    [SerializeField] protected float projectileSpeed;
    [SerializeField] protected float timeAttack;
    protected float baseHealth = 0f;

    [SerializeField] protected Transform projectileStart;
    [SerializeField] protected ParticleSystem fireParticle;
    [SerializeField] protected GameObject deathExplosion;

    protected Rigidbody rigid;
    protected IEnumerator enumRotate = null;

    public enum Type
    {
        Player,
        Enemy
    }

    public bool AlreadyAttack { get; set; } = false;
    public bool Dead { get; set; } = false;

    public float Health
    {
        get { return health; }
        set
        {
            if (value < Mathf.Epsilon)
            {
                health = 0f;
                Death();
            }
            else
            {
                health = value;
            }
        }
    }

    virtual public void ReceiveDamage(float damage)
    {
        Health -= damage;
    }

    protected void ResetAttack()
    {
        AlreadyAttack = false;
    }

    virtual public void Death()
    {
        Dead = true;
        Instantiate(deathExplosion, transform);
        rigid.velocity = Vector3.zero;
        rigid.useGravity = false;
    }
}