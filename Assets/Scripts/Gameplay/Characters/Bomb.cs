﻿using System;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public int points;
    [SerializeField] private float damage;
    [SerializeField] private GameObject body;
    [SerializeField] private GameObject explosion;
    [SerializeField] private LayerMask projectileMask;
    [SerializeField] private LayerMask tankMask;

    private Rigidbody rigid;
    public static Action<Bomb> OnBombDestroy;
    [HideInInspector] public bool destroyed = false;

    void Awake()
    {
        rigid = GetComponent<Rigidbody>();
    }

    void Start()
    {
        ChangeNormal();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!destroyed)
        {
            if (CheckLayerInMask(projectileMask, collision.gameObject.layer))
            {
                Destroy();
            }
            else if (CheckLayerInMask(tankMask, collision.gameObject.layer))
            {
                Tank tank = collision.gameObject.GetComponent<Tank>();
                tank.ReceiveDamage(damage);
                Destroy();
            }
        }
    }

    void ChangeNormal()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            Quaternion quatDestiny = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
            transform.rotation = quatDestiny;
        }
    }

    void Destroy()
    {
        destroyed = true;
        OnBombDestroy?.Invoke(this);

        rigid.velocity = Vector3.zero;
        rigid.useGravity = false;
        GetComponent<BoxCollider>().enabled = false;
        AudioManager.Get().Play("bomb");

        body.SetActive(false);
        GameObject explosionGO = Instantiate(explosion);
        explosionGO.transform.position = transform.position;
        explosionGO.transform.parent = transform;

        Destroy(gameObject, 2f);
    }

    public static bool CheckLayerInMask(LayerMask mask, int layer)
    {
        return mask == (mask | (1 << layer));
    }
}