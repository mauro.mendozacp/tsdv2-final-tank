﻿using System.Collections;
using UnityEngine;

public class Tank : Character
{
    [SerializeField] protected Type type;
    [SerializeField] protected GameObject tower;

    protected CapsuleCollider capsuleCollider;
    protected Animator animator;
    protected float alignmentSpeed = 5f;
    protected BulletPoolManager bulletPoolManager;

    protected void ChangeNormal()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            Quaternion quatDestiny = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, quatDestiny, alignmentSpeed * Time.deltaTime);
        }
    }

    protected void Attack(Vector3 focusPosition)
    {
        if (!AlreadyAttack)
        {
            AlreadyAttack = true;

            if (enumRotate != null)
            {
                StopCoroutine(enumRotate);
                enumRotate = null;
            }

            Vector3 target = focusPosition - transform.position;
            enumRotate = AimTarget(target.normalized);
            StartCoroutine(enumRotate);

            Invoke(nameof(ResetAttack), timeAttack);
        }
    }

    protected IEnumerator AimTarget(Vector3 target)
    {
        Quaternion toRot = Quaternion.LookRotation(target, transform.up);
        toRot.eulerAngles = new Vector3(0f, toRot.eulerAngles.y, 0f);

        while (tower.transform.rotation != toRot)
        {
            tower.transform.rotation = Quaternion.RotateTowards(tower.transform.rotation, toRot, aimSpeed * Time.deltaTime);

            yield return new WaitForEndOfFrame();
        }

        ShootBullet();
        yield return null;
    }

    void ShootBullet()
    {
        GameObject bulletGO = bulletPoolManager.GetBulletFromPool();
        bulletGO.transform.position = projectileStart.position;
        bulletGO.transform.rotation = projectileStart.rotation;

        Bullet bullet = bulletGO.GetComponent<Bullet>();
        bullet.owner = type;
        bullet.damage = damage;

        fireParticle.Play();
        AudioManager.Get().Play("bullet-launcher");

        Rigidbody bulletRigid = bulletGO.GetComponent<Rigidbody>();
        bulletRigid.AddForce(projectileStart.forward * projectileSpeed, ForceMode.Impulse);
    }

    override public void Death()
    {
        base.Death();
        capsuleCollider.enabled = false;
        animator.SetTrigger("Death");
    }
}
