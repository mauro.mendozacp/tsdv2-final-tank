﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Enemy : Tank
{
    [SerializeField] private float walkRange;
    [SerializeField] private float sightRange;
    [SerializeField] private float attackRange;
    public int points;

    private NavMeshAgent agent;
    [SerializeField] private LayerMask terrainMask;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private Canvas canvas;
    [SerializeField] private Image lifeBar;
    [HideInInspector] public Transform player;

    private Vector3 walkPoint;
    private bool walkPointSet;
    private float halfHeight;
    private float offsetY = 5f;

    public static Action<Enemy> OnEnemyDestroy;

    public enum State
    {
        Idle,
        Patrol,
        Follow,
        Attack,
        Death
    }
    private State state = State.Patrol;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        halfHeight = GetComponent<CapsuleCollider>().bounds.extents.y;
        bulletPoolManager = GameObject.FindGameObjectWithTag("BulletManager").GetComponent<BulletPoolManager>();

        canvas.worldCamera = Camera.main;
    }

    void Start()
    {
        baseHealth = Health;
        agent.speed = moveSpeed;
        agent.angularSpeed = turnSpeed;
    }

    void Update()
    {
        if (!Dead)
            UpdateStateMachine();
    }

    void FixedUpdate()
    {
        if (!Dead)
            StateMachine();

        BarFollowCamera();
    }

    void StateMachine()
    {
        switch (state)
        {
            case State.Idle:
                break;
            case State.Patrol: Patrol();
                break;
            case State.Follow: FollowPlayer();
                break;
            case State.Attack: Attack(player.position);
                break;
            case State.Death: agent.isStopped = true;
                break;
            default: throw new ArgumentOutOfRangeException();
        }
    }

    void Patrol()
    {
        if (!walkPointSet)
            SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }

    void SearchWalkPoint()
    {
        float posX = UnityEngine.Random.Range(-walkRange, walkRange);
        float posZ = UnityEngine.Random.Range(-walkRange, walkRange);

        walkPoint = transform.position;
        walkPoint.x += posX;
        walkPoint.y = offsetY;
        walkPoint.z += posZ;

        RaycastHit hit;
        if (Physics.Raycast(walkPoint, -transform.up, out hit, offsetY + halfHeight, terrainMask))
        {
            walkPoint.y = hit.point.y + (halfHeight / 2);
            walkPointSet = true;
        }
    }

    void FollowPlayer()
    {
        agent.SetDestination(player.position);
        transform.LookAt(player);
    }

    void UpdateStateMachine()
    {
        bool playerInSightRange = Physics.CheckSphere(transform.position, sightRange, playerMask);
        bool playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, playerMask);

        if (!playerInSightRange && !playerInAttackRange) state = State.Patrol;
        if (playerInSightRange && !playerInAttackRange) state = State.Follow;
        if (playerInSightRange && playerInAttackRange) 
        {
            agent.isStopped = true;
            state = State.Attack;
        }
        else
        {
            agent.isStopped = false;
        }
    }

    override public void ReceiveDamage(float damage)
    {
        if (!Dead)
        {
            base.ReceiveDamage(damage);
            UpdateLifeBar();

            if (Dead)
            {
                agent.isStopped = true;

                if (enumRotate != null)
                {
                    StopCoroutine(enumRotate);
                    enumRotate = null;
                }
            }
        }
    }

    void BarFollowCamera()
    {
        canvas.transform.LookAt(Camera.main.transform);
    }

    void UpdateLifeBar()
    {
        lifeBar.fillAmount = Health / baseHealth;
    }

    public void DeathAnimation()
    {
        OnEnemyDestroy?.Invoke(this);
        Destroy(gameObject);
    }
}