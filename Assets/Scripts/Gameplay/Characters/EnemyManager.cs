﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public int lenght;

    [HideInInspector] public List<Enemy> enemyList = new List<Enemy>();

    void Awake()
    {
        Enemy.OnEnemyDestroy += DestroyEnemy;
    }

    void OnDestroy()
    {
        Enemy.OnEnemyDestroy -= DestroyEnemy;
    }

    void DestroyEnemy(Enemy enemy)
    {
        enemyList.Remove(enemy);
    }
}
