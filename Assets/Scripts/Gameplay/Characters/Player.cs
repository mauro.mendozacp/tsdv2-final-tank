﻿using System;
using UnityEngine;

public class Player : Tank
{
    public int Score { get; set; }
    public int DestroyedBoxes { get; set; }
    public float DistanceTraveled { get; set; }

    public static Action<float, float> OnPlayerHealth;
    public static Action<int> OnPlayerScore;
    public static Action OnPlayerDestroyedBox;
    public static Action<int> OnReceiveScore;
    public static Action OnPlayerDeath;

    void Awake()
    {
        OnPlayerScore += SetScore;
        OnPlayerDestroyedBox += DestroyBox;

        animator = GetComponent<Animator>();
        rigid = GetComponent<Rigidbody>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        bulletPoolManager = GameObject.FindGameObjectWithTag("BulletManager").GetComponent<BulletPoolManager>();
    }

    void Start()
    {
        baseHealth = Health;
    }

    void Update()
    {
        if (!Dead)
        {
            Shoot();
        }
    }

    void FixedUpdate()
    {
        if (!Dead)
        {
            Move();
            Rotate();
            ChangeNormal();
        }
    }

    void OnDestroy()
    {
        OnPlayerScore -= SetScore;
        OnPlayerDestroyedBox -= DestroyBox;
    }

    void Move()
    {
        float ver = Input.GetAxis("Vertical");
        animator.SetFloat("Speed", Mathf.Abs(ver));

        if (Mathf.Abs(ver) > 0.0f)
        {
            rigid.AddForce(transform.forward * ver * moveSpeed, ForceMode.Force);
            DistanceTraveled += ver;
        }
    }

    void Rotate()
    {
        float hor = Input.GetAxis("Horizontal");
        if (Mathf.Abs(hor) > 0.0f)
        {
            transform.Rotate(new Vector3(0f, hor * turnSpeed, 0f) * Time.deltaTime);
        }
    }

    void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray castPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(castPoint, out hit))
            {
                Attack(hit.point);
            }
        }
    }

    override public void ReceiveDamage(float damage)
    {
        if (!Dead)
        {
            base.ReceiveDamage(damage);
            OnPlayerHealth?.Invoke(Health, baseHealth);
        }
    }

    void SetScore(int points)
    {
        Score += points;
        OnReceiveScore?.Invoke(Score);
    }

    void DestroyBox()
    {
        DestroyedBoxes++;
    }

    public void DeathAnimation()
    {
        OnPlayerDeath?.Invoke();
    }
}