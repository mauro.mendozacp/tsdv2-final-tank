﻿using UnityEngine;
using TMPro;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] private TMP_Text resultText;
    [SerializeField] private TMP_Text highScoreText;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private TMP_Text destroyedBoxesText;
    [SerializeField] private TMP_Text distanceTraveledText;
    private HighScore highScoreData;

    private float kilometerDivider = 1000f;
    private int highScore = 0;

    void Start()
    {
        highScoreData = GetComponent<HighScore>();
        highScore = highScoreData.GetHighScore();
        highScoreData.SaveScore();
        SetTexts();
    }

    void SetTexts()
    {
        if (GameManager.Get().Win)
        {
            AudioManager.Get().Play("win");
            resultText.text = "LEVEL COMPLETED!";
        }
        else
        {
            AudioManager.Get().Play("lose");
            resultText.text = "YOU FAIL!";
        }

        if (GameManager.Get().Score > highScore)
        {
            highScoreText.text = "NEW HIGHSCORE " + GameManager.Get().Score + "pts";
            scoreText.text = "";
        }
        else
        {
            highScoreText.text = "HIGHSCORE: " + highScore + "pts";
            scoreText.text = "SCORE: " + GameManager.Get().Score + "pts";
        }

        destroyedBoxesText.text = "Destroyed Boxes: " + GameManager.Get().DestroyedBoxes;
        distanceTraveledText.text = "Distance Traveled: " + (GameManager.Get().DistanceTraveled / kilometerDivider).ToString("F2") + "km";
    }

    public void BackToMenu()
    {
        GameManager.Get().ChangeScene(GameManager.SceneGame.MainMenu);
    }
}