﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighScoreUI : MonoBehaviour
{
    [SerializeField] private TMP_Text[] hsTexts = new TMP_Text[8];
    private string[] hsTextPref = { "1st", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th" };

    private HighScore highScoreData;

    void Awake()
    {
        highScoreData = GetComponent<HighScore>();
    }

    void Start()
    {
        SetScoreTexts();
    }

    void SetScoreTexts()
    {
        List<int> hsList = highScoreData.hsList;

        for (int i = 0; i < hsList.Count; i++)
        {
            if (i >= hsTexts.Length)
            {
                break;
            }

            hsTexts[i].text = hsTextPref[i] + ": " + hsList[i] + "pts";
        }
    }
}
