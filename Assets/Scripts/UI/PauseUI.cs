﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseUI : MonoBehaviour
{
    public void ContinueGame()
    {
        Time.timeScale = 1f;
        gameObject.SetActive(false);
    }

    public void BackToMenu()
    {
        Time.timeScale = 1f;
        GameManager.Get().ChangeScene(GameManager.SceneGame.MainMenu);
    }
}