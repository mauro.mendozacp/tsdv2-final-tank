﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUD : MonoBehaviour
{
    [SerializeField] private Image barImage;
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private TMP_Text timerText;
    [SerializeField] private GameObject pauseGO;

    void Awake()
    {
        Player.OnPlayerHealth += SetBar;
        Player.OnReceiveScore += SetScoreText;
    }

    void Start()
    {
        SetTimerText();
    }

    void Update()
    {
        SetTimerText();
    }

    void OnDestroy()
    {
        Player.OnPlayerHealth -= SetBar;
        Player.OnReceiveScore -= SetScoreText;
    }

    void SetBar(float health, float baseHealth)
    {
        barImage.fillAmount = health / baseHealth;
    }

    void SetScoreText(int score)
    {
        scoreText.text = score + "pts";
    }

    void SetTimerText()
    {
        int minutes = (int)(GameplayManager.Get().timer / 60);
        int seconds = (int)(GameplayManager.Get().timer % 60);

        timerText.text = minutes + ":";
        if (seconds < 10)
            timerText.text += "0";
        timerText.text += seconds;
    }

    public void PauseGame()
    {
        pauseGO.SetActive(true);
        Time.timeScale = 0f;
    }
}