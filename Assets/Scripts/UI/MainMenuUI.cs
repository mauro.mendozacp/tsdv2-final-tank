﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] private GameObject mainMenuGO;
    [SerializeField] private GameObject highScoreGO;
    [SerializeField] private GameObject creditsGO;

    void Start()
    {
        AudioManager.Get().Play("menu-music");
    }

    public void PlayGame()
    {
        GameManager.Get().ChangeScene(GameManager.SceneGame.GamePlay);
    }

    public void ShowHighScore()
    {
        mainMenuGO.SetActive(false);
        highScoreGO.SetActive(true);
        creditsGO.SetActive(false);
    }

    public void ShowCredits()
    {
        mainMenuGO.SetActive(false);
        highScoreGO.SetActive(false);
        creditsGO.SetActive(true);
    }

    public void ShowMenu()
    {
        mainMenuGO.SetActive(true);
        highScoreGO.SetActive(false);
        creditsGO.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
