using UnityEngine;

public class ButtonUI : MonoBehaviour
{
    public void Click()
    {
        AudioManager.Get().Play("click");
    }

    public void Hover()
    {
        AudioManager.Get().Play("hover");
    }
}