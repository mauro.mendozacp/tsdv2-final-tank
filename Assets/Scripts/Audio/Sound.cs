﻿using UnityEngine;

public enum TYPE
{
    SFX,
    MUSIC
}

[System.Serializable]
[CreateAssetMenu(fileName = "Sound", menuName = "Sound")]
public class Sound : ScriptableObject
{
    public AudioClip clip;

    public string _name;
    [Range(0f, 1f)]
    public float volume;
    [Range(.1f, 3f)]
    public float pitch;
    public bool loop;
    public TYPE type;

    [HideInInspector]
    public AudioSource source;
}