﻿using System;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviourSingleton<AudioManager>
{
    public AudioMixer masterMixer;
    [Range(0f, 1f)]
    public float volume = 1f;
    public Sound[] sounds;

    [HideInInspector] public bool sfxMuted = false;
    [HideInInspector] public bool musicMuted = false;
    float minVolume = -80f;
    float maxSfxVolume = 0f;
    float maxMusicVolume = -15f;
    float acceleratePitch = 1.25f;
    float normalPitch = 1.0f;

    void Start()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;

            switch (s.type)
            {
                case TYPE.SFX:
                    s.source.outputAudioMixerGroup = masterMixer.FindMatchingGroups("SFX")[0];
                    break;
                case TYPE.MUSIC:
                    s.source.outputAudioMixerGroup = masterMixer.FindMatchingGroups("Music")[0];
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(s.type), s.type, null);
            }

            if (s.volume > volume)
                s.source.volume = volume;
            else
                s.source.volume = s.volume;
        }
    }

    public Sound GetSound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound._name == name);
        if (s == null)
            Debug.LogWarning("Sound: " + name + " not found!");

        return s;
    }

    public void Play(string name)
    {
        Sound s = GetSound(name);
        if (s != null)
            s.source.Play();
    }

    public void Stop(string name)
    {
        Sound s = GetSound(name);
        if (s != null)
            s.source.Stop();
    }

    public void AllStop()
    {
        foreach (Sound s in sounds)
        {
            Stop(s._name);
        }
    }

    public void MuteSFX(bool mute)
    {
        sfxMuted = mute;
        float volume = mute ? minVolume : maxSfxVolume;
        masterMixer.SetFloat("sfxVol", volume);
    }

    public void MuteMusic(bool mute)
    {
        musicMuted = mute;
        float volume = mute ? minVolume : maxMusicVolume;
        masterMixer.SetFloat("musicVol", volume);
    }

    public void AccelerateMusicGameplay(string name, bool accelerate)
    {
        Sound s = GetSound(name);
        if (s != null)
            s.source.pitch = accelerate ? acceleratePitch : normalPitch;
    }
}