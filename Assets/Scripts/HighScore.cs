﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScore : MonoBehaviour
{
    [HideInInspector] public List<int> hsList = new List<int>();
    public string HighscorePath { get; } = "highscore.dat";

    void Awake()
    {
        GetScores();
    }

    public void SaveScore()
    {
        FileStream fs;

        if (!File.Exists(HighscorePath))
        {
            fs = File.Create(HighscorePath);
        }
        else
        {
            fs = File.Open(HighscorePath, FileMode.Append);
        }

        BinaryWriter bw = new BinaryWriter(fs);
        bw.Write(GameManager.Get().Score);

        fs.Close();
        bw.Close();
    }

    public void GetScores()
    {
        hsList = new List<int>();
        FileStream fs;

        if (File.Exists(HighscorePath))
        {
            fs = File.OpenRead(HighscorePath);
            BinaryReader br = new BinaryReader(fs);

            while (br.BaseStream.Position != br.BaseStream.Length)
            {
                hsList.Add(br.ReadInt32());
            }

            fs.Close();
            br.Close();

            hsList.Sort();
            hsList.Reverse();
        }
    }

    public int GetHighScore()
    {
        int highScore = 0;

        if (hsList.Count > 0)
            highScore = hsList[0];

        return highScore;
    }
}
