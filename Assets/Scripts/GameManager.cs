﻿using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourSingleton<GameManager>
{
    public int Score { get; set; } = 0;
    public int DestroyedBoxes { get; set; } = 0;
    public float DistanceTraveled { get; set; } = 0f;
    public bool GameOver { set; get; } = false;
    public bool Win { set; get; } = false;

    public enum SceneGame
    {
        MainMenu,
        GamePlay,
        GameOver
    }

    public void ChangeScene(SceneGame scene)
    {
        string sceneName;

        switch (scene)
        {
            case SceneGame.MainMenu:
                sceneName = "MainMenu";
                break;
            case SceneGame.GamePlay:
                sceneName = "Gameplay";
                break;
            case SceneGame.GameOver:
                sceneName = "Gameover";
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(scene), scene, null);
        }

        AudioManager.Get().AllStop();
        SceneManager.LoadScene(sceneName);
    }

    public void Init()
    {
        Score = 0;
        DestroyedBoxes = 0;
        DistanceTraveled = 0;
        Win = false;
        GameOver = false;
    }

    public void FinishGame(Player player, bool win)
    {
        Score = player.Score;
        DestroyedBoxes = player.DestroyedBoxes;
        DistanceTraveled = player.DistanceTraveled;
        Win = win;
        GameOver = true;

        AudioManager.Get().AllStop();
        ChangeScene(SceneGame.GameOver);
    }
}